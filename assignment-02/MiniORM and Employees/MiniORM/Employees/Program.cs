﻿using System;
using Employees.Model;

namespace Employees
{
	class Program
	{
		static void Main(string[] args)
		{
			string cS  = @"Data Source=(localdb)\MSSQLLocalDB; Database=Employees;";
			EmployeesDbContext db = new EmployeesDbContext(cS);

			Console.WriteLine("Departments:");
			foreach (var depart in db.Departments)
				Console.WriteLine("{0}, {1}", depart.Id, depart.Name);
			Console.WriteLine("\n");
			Console.WriteLine("Employees:");
			foreach (var em in db.Employees)
				Console.WriteLine("{0}, {1}, {2}", em.Id, em.Name, em.Family);
		}		
	}
}
