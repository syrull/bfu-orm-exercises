from C in Customers 
let bpl = from P in Products.Where ( P => P.UnitPrice < 5) 
select P.ProductID 
where !bpl.Except( from O in C.Orders from D in O.Order_Details select D.ProductID).Any() 
select C.CompanyName