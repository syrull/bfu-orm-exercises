(
	from E in Employees
	from O in E.Orders 
	from D in O.Order_Details 
	join P in Products on D.ProductID equals P.ProductID
	where P.ProductName == "Gravad Lax" || P.ProductName == "Mishi Kobe Niku" 
	select new {E.Title, E.FirstName, E.LastName}
).Distinct()