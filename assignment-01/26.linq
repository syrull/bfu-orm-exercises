from C in Customers 
from O in C.Orders 
from D in O.Order_Details 
join P in Products on D.ProductID equals P.ProductID 
orderby C.CompanyName, P.ProductName 
let as = (
	from D1 in Order_Details 
	where D1.ProductID == P.ProductID select D1.Quantity
).Cast<int>().Average() 
where D.Quantity > 5 * as 
select new { C.CompanyName, P.ProductName }