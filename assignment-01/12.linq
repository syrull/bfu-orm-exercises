(
	from E in Employees 
	from O in E.Orders 
	join C in Customers on O.CustomerID equals C.CustomerID 
	where E.City == C.City 
	select new { E.FirstName, E.LastName, E.City }
).Distinct()