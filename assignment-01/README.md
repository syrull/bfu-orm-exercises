# LINQ Assignments

Exercises: Northwind Database Querying with Linq

1. Give the name, address, city, and region of employees.
2. Give the name, address, city, and region of employees living in USA
3. Give the name, address, city, and region of employees older than 50 years old
4. Give the name, address, city, and region of employees that have placed orders to be delivered in Belgium. Write two versions of the query, with and without join.
5. Give the employee name and the customer name for orders that are sent by the company `Speedy Express` to customers who live in Brussels.
6. Give the title and name of employees who have sold at least one of the products `Gravad Lax` or `Mishi Kobe iku`.
7. Give the name and title of employees and the name and title of the person to whichthey refer (or null for the latter values if they don't refer to another employee).
8. Give the customer name, the product name and the supplier name for customerswho live in London and suppliers whose name is `Pavlova, Ltd.` or `Karkki Oy`.
9. Give the name of products that were bought or sold by people who live in London.Write two versions of the query, with and without union.
10. Give the names of employees who are strictly older than:(a) an employee who lives in London.(b) any employee who lives in London.
11. Give the name of employees who work longer than any employee of London.
12. Give the name of employees and the city where they live for employees who have sold to customers in the same city.
13. Give the name of customers who have not purchased any product.
14. Give the name of customers who bought all products with price less than 5.
15. Give the name of the products sold by all employees.
16. Give the name of customers who bought all products purchased by the customer whose identier is `LAZYK`
17. Give the name of customers who bought exactly the same products as the customer whose identier is `LAZYK`
18. Give the average price of products by category.
19. Given the name of the categories and the average price of products in each category.
20. Give the identier and the name of the companies that provide more than 3 products.
21. Give the identier, name, and number of orders of employees, ordered by the employee identier.
22. For each employee give the identier, name, and the number of distinct products sold, ordered by the employee identier.
23. Give the identier, name, and total sales of employees, ordered by the employee identier.
24. Give the identier, name, and total sales of employees, ordered by the employee identier for employees who have sold more than 70 dierent products.
25. Give the names of employees who sell the products of more than 7 suppliers.
26. Give the customer name and the product name such that the quantity of this product bought by the customer in a single order is more than 5 times the average quantity of this product bought in a single order among all clients.