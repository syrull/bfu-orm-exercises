from E in Employees orderby E.EmployeeID
select new { E.EmployeeID, E.FirstName, E.LastName,
	all = (
		from O in E.Orders 
		from D in O.Order_Details 
		let total = D.UnitPrice * D.Quantity select total
	)
	.Sum() 
}