from E in Employees 
join M1 in Employees on E.ReportsTo equals M1.EmployeeID into M2 from M in M2.DefaultIfEmpty() 
select new { 
	E.Title, E.FirstName, E.LastName,
	ManagerTitle = M.Title,
	ManagerFistName = M.FirstName,
	ManagerLastName = M.LastName 
} 