(
	from P in Products from D in P.Order_Details 
	join O in Orders on D.OrderID equals O.OrderID 
	join E in Employees on O.EmployeeID equals E.EmployeeID 
	join C in Customers on O.CustomerID equals C.CustomerID 
	where (E.City == "London") || (C.City == "London" ) 
	select new { P.ProductName }
).Distinct()