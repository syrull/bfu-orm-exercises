let cE = from E in Employees select E.EmployeeID 
where !cE.Except(
	from D in P.Order_Details
	join O in Orders on D.OrderID equals O.OrderID 
	join E in Employees on O.EmployeeID equals E.EmployeeID 
	select E.EmployeeID 
).Any() 
select P.ProductName