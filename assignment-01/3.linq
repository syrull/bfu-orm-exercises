from E in Employees
where DbFunctions.DiffYears(E.BirthDate, DateTime.Today) > 50
select new {E.FirstName, E.LastName, E.Address, E.City, E.Region}
