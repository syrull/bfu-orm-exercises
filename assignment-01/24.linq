 from E in Employees 
 let ap = (from O in E.Orders from D in O.Order_Details select D.ProductID).Distinct().Count()
 where ap > 70 orderby E.EmployeeID 
 select new { E.EmployeeID, E.FirstName, E.LastName,
	 asp = (
		 from O in E.Orders 
		 from D in O.Order_Details 
		 let total = D.UnitPrice * D.Quantity 
		 select total
	 ).Sum() 
 }