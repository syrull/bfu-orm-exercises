(
	from S in Suppliers 
	from P in S.Products 
	from D in P.Order_Details 
	join O in Orders on D.OrderID equals O.OrderID 
	join C in Customers on O.CustomerID equals C.CustomerID 
	where C.City == "London" && (S.CompanyName == "Pavlova, Ltd." || S.CompanyName == "Karkki Oy")
	select new { C.CompanyName, P.ProductName, SupplierName = S.CompanyName }
).Distinct()