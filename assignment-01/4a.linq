from E in Employees 
join O in Orders on E.EmployeeID equals O.EmployeeID 
where O.ShipCountry == "Belgium" 
select new { E.FirstName, E.LastName, E.Address, E.City, E.Region }