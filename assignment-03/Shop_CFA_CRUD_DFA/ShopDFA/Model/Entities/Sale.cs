﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopDFA.Model.Entities
{
    public class Sale
    {
        public int SaleId { get; set; }
        public virtual Product Product { get; set; }
        public int Quantity { get; set; }
        public string Client { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime Ordered { get; set; }
    }
}
