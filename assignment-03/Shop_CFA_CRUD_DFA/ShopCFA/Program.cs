﻿using ShopCFA.Model;
using ShopCFA.Model.Entities;
using System;

namespace ShopCFA
{
	class Program
	{
		static void Main(string[] args)
		{
            using (var db = new ShopContext())
            {


                var lego = db.Products.Add(new Product
                {
                    Name = "Lego",
                    Description = "Lego Building City",
                    Price = 1.0,
                    Created = DateTime.Now,
                    Updated = DateTime.Now
                });

                var fleshlight = db.Products.Add(new Product
                {
                    Name = "Fleshlight",
                    Description = "You know what it is",
                    Price = 0.0,
                    Created = DateTime.Now,
                    Updated = DateTime.Now
                });

                db.SaveChanges();

                db.Sales.Add(new Sale
                {
                    Product = lego.Entity,
                    Quantity = 3,
                    Client = "Dimitar",
                    Email = "t@t.t",
                    Phone = "80085",
                    Ordered = DateTime.Now
                });

                db.Sales.Add(new Sale
                {
                    Product = fleshlight.Entity,
                    Quantity = 99,
                    Client = "Johan",
                    Email = "t@e.c",
                    Phone = "80085",
                    Ordered = DateTime.Now
                });

                db.SaveChanges();

            }
        }
	}
}
